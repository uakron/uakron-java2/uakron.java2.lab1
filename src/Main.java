import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

// Filename: Main.java
// Date: September 16th, 2019
// Author: Bailey Parrish
// Description: Program that demonstrates the Month class.

public class Main
{
	public static void main(String[] args)
	{
		Month userMonth = new Month(getInput());
		Month currentMonth = new Month(getCurrentMonth());
		
		System.out.println("The month you entered was " + userMonth.toString() + ".");
		System.out.println("The current month is " + currentMonth.toString() + ".");
		
		if (userMonth.greaterThan(currentMonth))
		{
			System.out.println("The month you entered is later than the current month.");
		}
		else if (userMonth.equals(currentMonth))
		{
			System.out.println("The month you entered is the same as the current month.");
		}
		else
		{
			System.out.println("The month you entered is earlier than the current month.");
		}
	}
	
	// get the integer month value from the user
	public static int getInput()
	{
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter a month number value:");
		String monthInput = scanner.nextLine();
		
		while(!monthInput.matches("[0-9]+"))
		{
			System.out.println("Please enter numbers only!");
			monthInput = scanner.nextLine();
		}
		
		scanner.close();
		
		return Integer.parseInt(monthInput);
	}
	
	// get the current month integer value
	public static int getCurrentMonth()
	{
		Date today = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		
		return cal.get(Calendar.MONTH) + 1; // months are indexed at 0 by default, so add 1 when we return
	}
}