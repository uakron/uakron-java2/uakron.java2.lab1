// Filename: Month.java
// Date: September 16th, 2019
// Author: Bailey Parrish
// Description: Month class for Lab 1 of Java 2 course.

public class Month
{
	// stores the int value of the month
	int monthNumber;
	
	// no args constructer for the Month class
	public Month()
	{
		monthNumber = 1;
	}
	
	// int arg constructor for the Month class
	public Month(int month)
	{
		// handle invalid values
		if (month < 1 || month > 12)
		{
			monthNumber = 1;
		}
		else
		{
			monthNumber = month;
		}
	}
	
	// string arg constructor for the Month class
	public Month(String month)
	{
		// set the int month value depending on string value
		switch (month)
		{
			case ("January"):
				monthNumber = 1;
				break;
			case ("February"):
				monthNumber = 2;
				break;
			case ("March"):
				monthNumber = 3;
				break;
			case ("April"):
				monthNumber = 4;
				break;
			case ("May"):
				monthNumber = 5;
				break;
			case ("June"):
				monthNumber = 6;
				break;
			case ("July"):
				monthNumber = 7;
				break;
			case ("August"):
				monthNumber = 8;
				break;
			case ("September"):
				monthNumber = 9;
				break;
			case ("October"):
				monthNumber = 10;
				break;
			case ("November"):
				monthNumber = 11;
				break;
			case ("December"):
				monthNumber = 12;
				break;
			default:
				monthNumber = 1;
				break;
		}
	}
	
	// method to set the month number to new value
	public void setMonthNumber(int month)
	{
		// handle invalid values
		if (month < 1 || month > 12)
		{
			monthNumber = 1;
		}
		else
		{
			monthNumber = month;
		}
	}
	
	// get the int value of the month
	public int getMonthNumber()
	{
		return monthNumber;
	}
	
	// method to get the string value of the month depending on int value
	public String getMonthName()
	{
		switch (monthNumber)
		{
			case (1):
				return "January";
			case (2):
				return "February";
			case (3):
				return "March";
			case (4):
				return "April";
			case (5):
				return "May";
			case (6):
				return "June";
			case (7):
				return "July";
			case (8):
				return "August";
			case (9):
				return "September";
			case (10):
				return "October";
			case (11):
				return "November";
			case (12):
				return "December";
			default:
				return "January";
		}
	}
	
	// method that returns string value of corresponding int month value
	public String toString()
	{
		return getMonthName();
	}
	
	// method that compares two month int values; returns boolean true or false
	public boolean equals(Month month)
	{
		if (month.getMonthNumber() == getMonthNumber())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// method that compares if one month is greater than other; returns boolean true or false
	public boolean greaterThan(Month month)
	{
		if (getMonthNumber() > month.getMonthNumber())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// method that compares if one month is less than other; returns boolean true or false
	public boolean lessThan(Month month)
	{
		if (getMonthNumber() < month.getMonthNumber())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}