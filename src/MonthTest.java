// Filename: MonthTest.java
// Date: September 16th, 2019
// Author: Bailey Parrish
// Description: Unit tests for the Month class file.

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class MonthTest
{
	// assert that int 3 equals int 3
	@Test
	public void setMonthNumberTestTrue()
	{
		Month month = new Month();
		month.setMonthNumber(3);
		
		assertEquals(3, month.monthNumber);
	}
	
	// assert that int 9 does not equal int 3
	@Test
	public void setMonthNumberTestFalse()
	{
		Month month = new Month();
		month.setMonthNumber(9);
		
		assertNotEquals(3, month.monthNumber);
	}
	
	// assert that string April equals int 4
	@Test
	public void getMonthNumberTestTrue()
	{
		Month month = new Month("April");
		
		assertEquals(4, month.getMonthNumber());
	}
	
	// assert that string November does not equal int 4
	@Test
	public void getMonthNumberTestFalse()
	{
		Month month = new Month("November");
		
		assertNotEquals(4, month.getMonthNumber());
	}
	
	// assert that int 12 equals string December
	@Test
	public void getMonthNameTestTrue()
	{
		Month month = new Month(12);
		
		assertEquals("December", month.getMonthName());
	}
	
	// assert that int 2 does not equal string December
	@Test
	public void getMonthNameTestFalse()
	{
		Month month = new Month(2);
		
		assertNotEquals("December", month.getMonthName());
	}
	
	// assert that int 4 outputs string April
	@Test
	public void toStringTestTrue()
	{
		Month month = new Month(4);
		
		assertEquals("April", month.toString());
	}
	
	// assert that int 4 does not output string December
	@Test
	public void toStringTestFalse()
	{
		Month month = new Month(4);
			
		assertNotEquals("December", month.toString());
	}
	
	// assert that string August equals int 8
	@Test
	public void equalsTestTrue()
	{
		Month month1 = new Month("August");
		Month month2 = new Month(8);
		
		assertTrue(month1.equals(month2));
	}
	
	// assert that string August does not equal int 7
	@Test
	public void equalsTestFalse()
	{
		Month month1 = new Month("August");
		Month month2 = new Month(7);
			
		assertFalse(month1.equals(month2));
	}
	
	// assert that string December is greater than int 4
	@Test
	public void greaterThanTestTrue()
	{
		Month month1 = new Month("December");
		Month month2 = new Month(4);
		
		assertTrue(month1.greaterThan(month2));
	}
	
	// assert that string March is not greater than int 6
	@Test
	public void greaterThanTestFalse()
	{
		Month month1 = new Month("March");
		Month month2 = new Month(6);
		
		assertFalse(month1.greaterThan(month2));
	}
	
	// assert that int 2 is less than string May
	@Test
	public void lessThanTestTrue()
	{
		Month month1 = new Month(2);
		Month month2 = new Month("May");
		
		assertTrue(month1.lessThan(month2));
	}
	
	// assert that int 5 is not less than string January
	@Test
	public void lessThanTestFalse()
	{
		Month month1 = new Month(5);
		Month month2 = new Month("January");
		
		assertFalse(month1.lessThan(month2));
	}
}
